package edu.westga.cs1302.bindingexample.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * Controller for the UI interaction.
 *
 * @author CS1302
 */
public class Controller {

	@FXML
	private TextField characterNameTextField;

	@FXML
	private TextField actorNameTextField;

	@FXML
	private Label characterNameSummaryLabel;

	@FXML
	private Label actorNameSummaryLabel;

	/**
	 * Instantiates a new controller.
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	public Controller() {

	}

	@FXML
	private void initialize() {
		this.characterNameSummaryLabel.textProperty().bind(this.characterNameTextField.textProperty());
		this.actorNameSummaryLabel.textProperty().bind(this.actorNameTextField.textProperty());
	}

}